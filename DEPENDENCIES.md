* caosdb-server >= 0.12.0
* Python >= 3.9
* pip >= 20.0.2

Any other dependencies are defined in the setup.py and are being installed via pip
