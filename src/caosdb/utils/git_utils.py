
from linkahead.utils.git_utils import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.git_utils`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
