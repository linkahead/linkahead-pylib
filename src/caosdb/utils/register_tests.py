
from linkahead.utils.register_tests import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.register_tests`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
