
from linkahead.utils.server_side_scripting import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.server_side_scripting`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
