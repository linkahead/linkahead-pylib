
from linkahead.utils.checkFileSystemConsistency import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.utils.checkFileSystemConsistency`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
