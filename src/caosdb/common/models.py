
from linkahead.common.models import *
from warnings import warn

warn(("CaosDB was renamed to LinkAhead. Please import this library as `import linkahead.common.models`. Using the"
      " old name, starting with caosdb, is deprecated."), DeprecationWarning)
