
PyLinkAhead Code Gallery
========================

This chapter collects code examples which can be immediately run against an empty LinkAhead instance.

.. note::

   These examples require a configuration file with valid server and user/password settings.  Refer
   to the :ref:`Configuration <Configuration of PyLinkAhead>` section for details.

.. toctree::
   :maxdepth: 2
   :caption: The code examples:

   simulation
   curator-permissions
