# Changelog #

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.17.0] - 2025-01-14 ##

### Added ###

* New setup extra `test` which installs the dependencies for testing.
* The Container class has a new member function `filter_by_identity`
  which is based on `_filter_entity_list`.
* The `Entity` properties `_cuid` and `_flags` are now available for read-only access
  as `cuid` and `flags`, respectively.

### Changed ###

* Renamed the `filter` function of Container, ParentList and
  PropertyList to `filter_by_identity`.

### Deprecated ###

* `ParentList.filter` and `PropertyList.filter` functions, use
  `filter_by_identity` instead.

### Removed ###

* Support for Python 3.8

### Fixed ###

* [#73](https://gitlab.com/linkahead/linkahead-pylib/-/issues/73)
  `Entity.to_xml` now detects potentially infinite recursion and prevents an error
* [#89](https://gitlab.com/linkahead/linkahead-pylib/-/issues/89)
  `to_xml` does not add `noscript` or `TransactionBenchmark` tags anymore
* [#103](https://gitlab.com/linkahead/linkahead-pylib/-/issues/103)
  `authentication/interface/on_response()` does not overwrite
  `auth_token` if new value is `None`
* [#119](https://gitlab.com/linkahead/linkahead-pylib/-/issues/119)
  The diff returned by compare_entities now uses id instead of name as
  key if either property does not have a name
* [#87](https://gitlab.com/linkahead/linkahead-pylib/-/issues/87)
  `XMLSyntaxError` messages when parsing (incomplete) responses in
  case of certain connection timeouts.
  The diff returned by compare_entities now uses id instead of name as key if either property does not have a name
* [#127](https://gitlab.com/linkahead/linkahead-pylib/-/issues/127)
  pylinkahead.ini now supports None and tuples as values for the `timeout` keyword

## [0.16.0] - 2024-11-13 ##

### Added ###

* `ParentList` and `PropertyList` now have a `filter` function that allows to select a subset of
  the contained elements by ID and/or name.
* Official support for Python 3.13
* Added arguments to `describe_diff` that allow customizing the labels for the 'old' and the 'new' diffs.
* Optional `realm` argument for `linkahead_admin.py set_user_password`
  which defaults to `None`, i.e., the server's default realm.

### Changed ###

* `compare_entities` is now case insensitive with respect to property and
  recordtype names
* `_ParentList` is now called `ParentList`
* `_Properties` is now called `PropertyList`
* `ParentList.remove` is now case insensitive when a name is used.

### Deprecated ###

* the use of the arguments `old_entity` and `new_entity` in `compare_entities`
  is now deprecated. Please use `entity0` and `entity1` respectively instead.

### Fixed ###

* [gitlab.indiscale.com#200](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/200)
  ``linkahead_admin.py`` prints reasonable error messages when users
  or roles don't exist.

## [0.15.1] - 2024-08-21 ##

### Deprecated ###

* `connection.get_username`. Use `la.Info().user_info.name` instead.

### Fixed ###

* [#128](https://gitlab.com/linkahead/linkahead-pylib/-/issues/128)
  Assign `datetime.date` or `datetime.datetime` values to `DATETIME`
  properties.

### Documentation ###

* Added docstrings for `linkahead.models.Info` and `linkahead.models.UserInfo`.

## [0.15.0] - 2024-07-09 ##

### Added ###

* Support for Python 3.12
* The `linkahead` module now opts into type checking and supports mypy.
* [#112](https://gitlab.com/linkahead/linkahead-pylib/-/issues/112)
  `Entity.update_acl` now supports optional `**kwargs` that are passed to the
  `Entity.update` method that is called internally, thus allowing, e.g.,
  updating the ACL despite possible naming collisions with `unique=False`.
* a `role` argument for `get_entity_by_name` and `get_entity_by_id`

### Changed ###

* Using environment variable PYLINKAHEADINI instead of PYCAOSDBINI.

### Removed ###

* Support for Python 3.7

### Fixed ###

* [#104](https://gitlab.com/linkahead/linkahead-pylib/-/issues/104) Selecting
  parts of a `Container` with a `slice` used to return a `list` object instead
  of a `Container`, removing all useful methods of the `Container` class. This
  has been fixed and using a `slice` such as `[:2]` now returns a new
  `Container`.
* [#120](https://gitlab.com/linkahead/linkahead-pylib/-/issues/120) Unwanted
  subproperties in reference properties.

### Documentation ###

* Added documentation and a tutorial example for the usage of the `page_length`
  argument of `execute_query`.

## [0.14.0] - 2024-02-20

### Added ###

* `utils.merge_entities` now has a `merge_id_with_resolved_entity` keyword
  which allows to identify property values with each other in case that one is
  an id and the other is an Entity with this id. Default is ``False``, so no
  change to the default behavior.
* `apiutils.escape_quoted_text` for escaping text in queries.

### Changed ###

* `cached_query()` now also caches uniqueness related exceptions.

## [0.13.2] - 2023-12-15

### Fixed ###

* [#113](https://gitlab.com/linkahead/linkahead-pylib/-/issues/113) Container could fail to delete when there were reference properties.
* HTTP status 431 (Headers too long) now also raises an URI too long exception.

## [0.13.1] - 2023-10-11 ##

### Fixed ###

* no Error when no configuration file is used
  [Issue](https://gitlab.com/linkahead/linkahead-pylib/-/issues/107)

## [0.13.0] - 2023-10-10 ##

### Added ###

* New `page_length` parameter for `caosdb.execute_query` and
  `caosdb.Query.execute`. See docstrings for more details.
* `Entity.remove_value_from_property` function that removes a given value from a
  property and optionally removes the property if it is empty afterwards.

### Changed ###

* `_Messages` is now `Messages` and inherits from list instead of dict
* `Message.__init__` signature changed and `type` defaults to "Info" now.
* `Message.__eq__` changed. Equality is equality of `type`, `code`, and
  `description` now.
* Rename from CaosDB to LinkAhead.  For proper migration, follow the instructions
  in `migration_to_linkahead.md` and check the documentation at [docs.indiscale.com](https://docs.indiscale.com/caosdb-pylib/README_SETUP.html#migration).

### Deprecated ###

* The API of Messages has been simplified and some ways to interact with
  messages have been deprecated. Warnings are raised correspondingly.
* `Message.get_code`. Use the `code` property instead.

### Fixed ###

- Detection for cyclic references when converting entites using the high level API.

## [0.12.0] - 2023-06-02 ##

### Added ###

- Added location argument to  `src/caosdb/utils/checkFileSystemConsistency.py`
- Entity getters: `get_entity_by_<name/id/path>`
- Cached versions of entity getters and of `execute_query` (`cached_query`)

### Deprecated ###

- getOriginUrlIn, getDiffIn, getBranchIn, getCommitIn (formerly apiutils) have been
  moved to caosdb.utils.git_utils

### Fixed ###

- Fixed `src/caosdb/utils/checkFileSystemConsistency.py`

### Documentation ###

* [#83](https://gitlab.com/caosdb/caosdb-pylib/-/issues/83) - Improved
  documentation on adding REFERENCE properties, both in the docstring of
  `Entity.add_property` and in the data-insertion tutorial.

## [0.11.2] - 2023-03-14 ##

### Fixed ###
- root logger is no longer used to create warnings. Fixes undesired output in
  stderr

## [0.11.1] - 2023-03-07 ##
(Florian Spreckelsen)

### Changed ###

* Renamed `caosdb.common.models._Parents` to `caosdb.common.models._ParentList`.

### Fixed ###

* [caosdb-pylib#90](https://gitlab.com/caosdb/caosdb-pylib/-/issues/90): `Entity.get_parents_recursively()` did not work for unretrieved parents.

## [0.11.0] - 2023-01-19 ##
(Florian Spreckelsen)

### Added ###

* `apiutils.EntityMergeConflictError` class for unresesolvable merge conflicts
  when merging two entities
* Re-introduced support for Python 3.7

### Changed ###

* `apiutils.merge_entities` now raises an `EntityMergeConflictError` in case of
  unresolvable merge conflicts.

### Fixed ###

* [#82](https://gitlab.com/caosdb/caosdb-pylib/-/issues/82) Merging an entity
  with properties with missing datatype leads to Exception - The correct
  exception is raised in case of a missing LIST datatype.

### Documentation ###

* [Fixed](https://gitlab.com/caosdb/caosdb-pylib/-/issues/79)
  `{action}_entity_permissions` help line.

## [0.10.0] - 2022-11-14
(Florian Spreckelsen)

### Added ###

* HTTP connections are allowed additionally to HTTPS connections.
* Dependency on the `requests` package.
* Dependency on the `python-dateutil` package.
* `Connection.https_proxy` and `Connection.http_proxy` option of the
  pycaosdb.ini and the `https_proxy` and `http_proxy` parameter of the
  `configure_connection` function. See the documentation of the
  latter for more information.
  Note that the `HTTP_PROXY` and `HTTPS_PROXY` environment variables are
  respected as well, unless overridden programmatically.
* `apiutils.empty_diff` function that returns `True` if the diffs of two
  entities found with the `compare_entitis` function are empty, `False`
  otherwise.

### Changed ###

* `apiutils.compare_entities` now has an optional `compare_referenced_records`
  argument to compare referenced Entities recursively (fomerly, only the
  referenced Python objects would be compared). The default is `False` to
  recover the original behavior.
* `apiutils.merge_entities` now has an optional
  `merge_references_with_empty_diffs` argument that determines whether a merge
  of two entities will be performed if they reference identical records (w.r.t
  th above `empty_diff` function). Formerly this would have caused a merge
  conflict if the referenced record(s) were identical, but stored in different
  Python objects.
* `apiutils.merge_entities` now has an optional `force` argument (defaults to
  `False`, i.e., the old behavior) which determines whether in case of merge
  conflicts errors will be raised or the properties and attributes of entity A
  will be overwritten by entity B.

### Deprecated ###

* `Connection.socket_proxy` option of the pycaosdb.ini. Please use
  `Connection.https_proxy` or `Connection.http_proxy` instead. The deprecated
  option will be removed with the next minor release.

### Fixed ###

* handling of special attributes (name, id, ...) in `apiutils.empty_diff`

## [0.9.0] - 2022-10-24
(Florian Spreckelsen)

### Added ###

* Add TimeZone class and parse the server's time zone in the Info response.

### Fixed ###

* [#141](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/141)
  `password_method = unauthenticated` not allowed by schema
* Set PyYAML dependency back to PyYaml>=5.4.1 (from 6.0) for better
  compatibility with docker-compose

### Documentation ###

* Added curator role permissions example to code gallery

## [0.8.0] - 2022-07-12
(Timm Fitschen)

### Removed ###

* Support for Python 3.6 and Python 3.7

### Fixed ###

* `read()` of MockupResponse returns now an appropriate type on modern systems
* [caosdb-server#142](https://gitlab.com/caosdb/caosdb-server/-/issues/142)
  Can't create users with dots in their user names

## [0.7.4] - 2022-05-31
(Florian Spreckelsen)

### Fixed ###

* [#64](https://gitlab.com/caosdb/caosdb-pylib/-/issues/64) Use `Dict[]` and
  `List[]` from `typing` for type hinting instead of `dict[]` and `list[]` for
  compatibility with Python<3.9.

## [0.7.3] - 2022-05-03
(Henrik tom Wörden)

### Added ###

- New function in apiutils that copies an Entity.
- New EXPERIMENTAL module `high_level_api` which is a completely refactored version of
  the old `high_level_api` from apiutils. Please see the included documentation for details.
- `to_graphics` now has  `no_shadow` option.

### Changed ###

- Added additional customization options to the plantuml module.
- The to_graphics function in the plantuml module uses a temporary directory now for creating the output files.

### Fixed ###

* [#75](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/75), [#103](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/103) Fixed JSON schema to allow more sections, and correct requirements for
  password method.

## [0.7.2] - 2022-03-25 ##
(Timm Fitschen)

### Deprecated ###

* In module `caosdb.apiutils`:
  * `CaosDBPythonEntity` class
  * `convert_to_entity` function
  * `convert_to_python_object` function

### Fixed ###

* [caosdb-pylib#106](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/106)
  Parsing Error in class caosdb.common.models.ACL. This may lead to the
  unintentional revocation of permissions for some users or roles during
  updates. However, no additional permissions are being granted.

### Documentation ###

## [0.7.1] - 2022-03-11 ##
(Daniel Hornung)

### Documentation ###

- `timeout` option in example pycaosdb.ini

## [0.7.0] - 2022-01-21 ##

### Added ###

- Function in administration that generates passwords that comply with the rules.

### Fixed ###

- #90 compare_entities function in apiutils does not check units
- #94 some special properties were not checked in compare_entities

### Security ###

## [0.6.1] - 2021-12-03 ##

### Fixed ###

- #50 keyring can be used as password input method again
* #81 compare_entities from apiutils does not compare entity values

## [0.6.0] - 2021-10-19 ##

### Added ###

- It is possible now to supply a password for caosdb_admin on the command line
  and also activate the user directly using "-c".
* Added examples for complex data models to documentation
* extended apiutils with `resolve_reference(Property)`
* is_reference function for Properties
* function `retrieve_substructure` that recursively adds connected entities.

### Changed ###

* Retrievals of entities where the class does not match the entity role raise
  a ValueError now. See
  [here](https://gitlab.indiscale.com/caosdb/src/caosdb-pylib/-/issues/66) for more
  information. Updating a role is now being done by setting the `Entity.role`
  to the new role (as string).
* Entity.add_property and Entity.add_parent do not accept `**kwargs`-style
  keywords anymore. Formerly known keywords have been refactored into named
  parameters.
* [#35](https://gitlab.com/caosdb/caosdb-pylib/-/issues/35) Loggers now use the
  name of the unit where they are called instead of a static name

### Deprecated ###

* `id_query(ids)` in apiutils (to be removed with >=0.5.4)
* The whole yamlapi with the following functions (to be removed with >=0.5.4):
  * `append_sublist`
  * `kv_to_xml`
  * `dict_to_xml`
  * `yaml_to_xml`
  * `process`
  * `yaml_file_to_xml`
  * `insert_yaml_file`

### Fixed ###

* #60 Unintuitive behavior of `Entity.role` after a `Entity(id).retrieve()`
  Originally the role was `None`. The correct role is present now.
* #53 Documentation of inheritance
* #38 Dependencies in chunk-deletion of containers

### Security ###

## [0.5.2] - 2021-06-03 ##

### Added ###

* Entity State support (experimental, no StateModel support yet). See the
  `caosdb.State` class for more information.
* `etag` property for the `caosdb.Query` class. The etag allows to debug the
  caching and to decide whether the server has changed between queries.
* function `_read_config_files` to read `pycaosdb.ini` files from different paths.

### Changed ###

* Updated error-handling tutorial in documentation to reflect the new
  error classes

### Fixed ###
* #45 - test_config_ini_via_envvar

### Security ###

## [0.5.1] - 2021-02-12 ##

### Fixed ###

* #43 - Error with `execute_query` when server doesn't support query caching.

## [0.5.0] - 2021-02-11 ##

### Added ###

* New exceptions `HTTPForbiddenException` and
  `HTTPResourceNotFoundException` for HTTP 403 and 404 errors,
  respectively.
* `BadQueryError`, `EmptyUniqueQueryError`, and `QueryNotUniqueError`
  for bad (unique) queries.
* Added `cache` paramter to `execute_query` and `Query.execute` which indicates
  whether server may use the cache for the query execution.
* Added `cached` property to the `Query` class which indicates whether the
  server used the cache for the execution of the last query.
* Documentation moved from wiki to this repository and enhanced.

### Changed ###

* Renaming of `URITooLongException` to `HTTPURITooLongError`.
* Raising of entity exceptions and transaction errors. Whenever any
  transaction fails, a `TransactionError` is raised. If one ore more
  entities caused that failure, corresponding entity errors are
  attached as direct and indirect children of the
  `TransactionError`. They can be accessed via the `get_errors`
  (direct children) and `get_all_errors` (direct and indirect
  children) methods; the causing entities are accessed by
  `get_entities` and `get_all_entities`. The `has_error` method can be
  used to check whether a `TransactionError` was caused by a specific
  `EntityError`.
* Unique queries will now result in `EmptyUniqueQueryError` or
  `QueryNotUniqueError` if no or more than one possible candidate is
  found, respectively.

### Removed ###

* Dynamic exception type `EntityMultiError`.
* `get_something` functions from all error object in `exceptions.py`
* `AmbiguityException`

## [0.4.1] - 2021-02-10 ##

### Added ###

* Versioning support (experimental). The version db.Version class can
  represents particular entity versions and also the complete history of an
  entity.
* Automated documentation builds: `make doc`

### Fixed ###

* deepcopy of `_Messages` objects

## [0.4.0] - 2020-07-17##

### Added ###

* `[Entity|Container].get_property_values` for deeply nested references, e.g.
  from results of SELECT queries.
* two new `password_method`s for the `pycaosdb.ini` and the
  `configure_connection` function: `unauthenticated` for staying
  unauthenticated (and using the anonymous user) and `auth_token`. If
  `password_method == "auth_token"` the `auth_token` option is required as
  well.
* Empty string support (See caosdb-server#33)

### Changed ###

* `get_property` method also accepts instances of properties now, e.g.
  `record.get_property(Property(name="length"))`
* the value of properties is parsed to python types (int, float, boolean) when
  setting the value with the setter and when the datatype changes. Before this
  change, the value was parsed to python types only when parsing an xml and
  only for int and float.

### Deprecated ###

* Setting the `auth_token` option without setting the `password_method` to
  `auth_token`. This affects both the `pycaosdb.ini` and the
  `configure_connection` function. During the deprecation phase it will be
  assumed that `password_method` is `auth_token` if the the `auth_token` is
  set.

### Fixed ###

- Replaced deprecated Logger.warn() method.

## [0.3.0] - 2020-04-24##

### Added ###

* `apiutils.apply_to_ids` -- a helper which applies a function to all ids which
  are used by an entity (own entity, parents, properties, references etc.).

### Fixed ###

* import bugs in apiutils

## [0.2.4] - 2020-04-23

### Added

- An `auth_token` parameter for `caosdb.configure_connection(...)`. This
  parameter accepts a plain text auth token (which can only be issued by the
  CaosDB Server). Under the hood, auth tokens are stored plain, instead of
  urlencoded now.
- New type of exception: `ConfigurationException` for misconfigurations.
- Some unit tests, mainly for the `caosdb.connection.authentication` module
- Advanced setup.py for easy versioning and publication as pypi repository.
- `caosdb.apiutils` has new functions `id_query` and
  `retrieve_entities_with_ids`
- New exception: `EntityDoesNotExistError`
- `RELEASE_GUIDELINES.md` with release instructions.
- More documentation and tests.

### Changed

- [pytest](https://docs.pytest.org/en/latest/) is the new preferred unit test
  frame work.
- If a password is specified in the configuration even though the
  password_method is not set to `plain`, a warning is logged.
- Under the hood, the password of from a `pass` or `keyring` call is not stored
  anymore. Instead the password is requested each time a login is necessary.
- Always load system default CA repository into the ssl context.
- Datatypes are now in `caosdb.common.datatype` (though still available in the
  main `caosdb` namespace).
- Logging to stdout is now more configurable.

### Deprecated

- Unit test frame work: [Nose](https://nose.readthedocs.io/en/latest/) should
  not be used anymore. Please use [pytest](https://docs.pytest.org/en/latest/)
  instead.

### Fixed

- #1 - Problems with pass as a credentials provider
- #3 - Python client does login before the first request to circumvent problems
  with anonymous role.
- Many other fixes


## [0.1.0] - 2018-10-09 ##

Tag `v0.1` - Commit 6fc0dcaa


### Added
- everything
