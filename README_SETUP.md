# Getting started with PyCaosDB #

## Installation ##

### How to install ###

First ensure that python with at least version 3.9 is installed. Should this not be
the case, you can use the [Installing python](#installing-python-) guide for your OS.

#### Generic installation ####

To install this LinkAhead python client locally, use `pip`/`pip3`:

```sh
pip install linkahead
```

#### Additional dependencies ####

To test using tox, you also need to install tox:  
`pip install tox`  

To install dependencies used by optional functionality, the following pip extras 
keywords are defined:
- `test` for testing with pytest
- `mypy` for mypy and types
- `jsonschema` 
- `keyring` 

These extras can be installed using: 
```sh
pip install .[KEYWORD]
```
A current list of the dependencies installed with this program as well as those installed with 
the keywords can be found in `setup.py`s `setup_package()` method, in the `metadata` dictionary
entries `install_requires` and `extras_require`.

### Installing python ###

#### Linux ####

Make sure that Python (at least version 3.9) and pip is installed, using your system tools and
documentation.

Then open a terminal and continue in the [Generic installation](#generic-installation) section.

#### Windows ####

If a Python distribution is not yet installed, we recommend Anaconda Python, which you can download
for free from [https://www.anaconda.com](https://www.anaconda.com).  The "Anaconda Individual Edition" provides most of all
packages you will ever need out of the box.  If you prefer, you may also install the leaner
"Miniconda" installer, which allows you to install packages as you need them.

After installation, open an Anaconda prompt from the Windows menu and continue in the [Generic
installation](#generic-installation) section.

#### MacOS ####

If there is no Python 3 installed yet, there are two main ways to
obtain it: Either get the binary package from
[python.org](https://www.python.org/downloads/) or, for advanced
users, install via [Homebrew](https://brew.sh/). After installation
from python.org, it is recommended to also update the TLS certificates
for Python (this requires administrator rights for your user):

```sh
# Replace this with your Python version number:
cd /Applications/Python\ 3.9/

# This needs administrator rights:
sudo ./Install\ Certificates.command
```

After these steps, you may continue with the [Generic installation](#generic-installation) section.

## Configuration ##

The configuration is done using `ini` configuration files.  The content of these configuration files
is described in detail in the [configuration section of the documentation](https://docs.indiscale.com/caosdb-pylib/configuration.html).

## Try it out ##

Start Python and check whether you can access the database. (You will be asked for the
password):

```python
In [1]: import caosdb as db
In [2]: db.Info()
Please enter the password:  # It's `caosdb` for the demo server.
Out[2]: Connection to CaosDB with 501 Records.
```

Note: This setup will ask you for your password whenever a new connection is created. If you do not
like this, check out the "Authentication" section in the [configuration documentation](configuration.md).

Now would be a good time to continue with the [tutorials](tutorials/index).

## Run Unit Tests

- Run all tests: `tox` or `make unittest`
- Run a specific test file: e.g. `tox -- unittests/test_schema.py`
- Run a specific test function: e.g. `tox -- unittests/test_schema.py::test_config_files`
- To run using pytest: `pytest .`

## Documentation ##
We use sphinx to create the documentation. Docstrings in the code should comply
with the Googly style (see link below).

Build documentation in `build/` with `make doc`.

### How to contribute ###

- [Google Style Python Docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
- [Google Style Python Docstrings 2nd reference](https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings)
- [References to other documentation](https://www.sphinx-doc.org/en/master/usage/extensions/intersphinx.html#role-external)

### Troubleshooting ###
If the client is to be executed directly from the `/src` folder, an initial `.\setup.py install` must be called.

## Migration ##
TODO
