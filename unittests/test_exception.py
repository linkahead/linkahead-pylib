# -*- encoding: utf-8 -*-
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2023 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2023 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import warnings

from linkahead.exceptions import (CaosDBConnectionError, CaosDBException,
                                  LinkAheadConnectionError, LinkAheadException)

# make sure the deprecation is raised
with warnings.catch_warnings(record=True) as w:
    # Cause all warnings to always be triggered.
    warnings.simplefilter("always")
    CaosDBException('1')

    assert issubclass(w[-1].category, DeprecationWarning)
    assert "The name CaosDBException is deprecated" in str(w[-1].message)
    CaosDBConnectionError('1')

    assert issubclass(w[-1].category, DeprecationWarning)
    assert "The name CaosDBConnectionError is deprecated" in str(w[-1].message)


# make sure the deprecated Error still allows to catch exceptions
def raiseCE():
    raise CaosDBException('a')


def raiseCCE():
    raise CaosDBConnectionError('a')


# Exception must be caught
try:
    raiseCE()
except CaosDBException as e:
    print(e.msg)

# Exception must be caught
try:
    raiseCCE()
except CaosDBConnectionError as e:
    print(e.msg)
