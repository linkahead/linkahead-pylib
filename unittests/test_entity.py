# -*- encoding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Tests for the Entity class."""
import os
# pylint: disable=missing-docstring
import unittest

import linkahead
from linkahead import (INTEGER, Entity, Parent, Property, Record, RecordType,
                       configure_connection)
import warnings
from linkahead.common.models import SPECIAL_ATTRIBUTES
from linkahead.connection.mockup import MockUpServerConnection
from lxml import etree
from pytest import raises

UNITTESTDIR = os.path.dirname(os.path.abspath(__file__))


class TestEntity(unittest.TestCase):

    def setUp(self):
        self.assertIsNotNone(Entity)
        configure_connection(url="unittests", username="testuser",
                             password_method="plain",
                             password="testpassword", timeout=200,
                             implementation=MockUpServerConnection)

    def test_instance_variables(self):
        entity = Entity()
        self.assertTrue(hasattr(entity, "role"))
        self.assertIsNone(entity.role)
        self.assertTrue(hasattr(entity, "id"))
        self.assertTrue(hasattr(entity, "name"))
        self.assertTrue(hasattr(entity, "description"))
        self.assertTrue(hasattr(entity, "parents"))
        self.assertTrue(hasattr(entity, "properties"))

    def test_entity_role_1(self):
        entity = Entity(role="TestRole")
        self.assertEqual(entity.role, "TestRole")
        entity.role = "TestRole2"
        self.assertEqual(entity.role, "TestRole2")

    def test_entity_role_2(self):
        entity = Entity()

        self.assertIsNone(entity.role)
        self.assertEqual(entity.to_xml().tag, "Entity")

        entity.role = "Record"
        self.assertEqual(entity.role, "Record")
        self.assertEqual(entity.to_xml().tag, "Record")

    def test_recordtype_role(self):
        entity = RecordType()

        self.assertEqual(entity.role, "RecordType")
        self.assertEqual(entity.to_xml().tag, "RecordType")

    def test_property_role(self):
        entity = Property()

        self.assertEqual(entity.role, "Property")
        self.assertEqual(entity.to_xml().tag, "Property")

    def test_instantiation(self):
        e = Entity()
        for attr in SPECIAL_ATTRIBUTES:
            assert hasattr(e, attr)

    def test_instantiation_bad_argument(self):
        with self.assertRaises(Exception):
            Entity(rol="File")

    def test_parse_role(self):
        """During parsing, the role of an entity is set explicitely. All other
        classes use the class name as a "natural" value for the role property.
        """
        parser = etree.XMLParser(remove_comments=True)
        entity = Entity._from_xml(Entity(),
                                  etree.parse(os.path.join(UNITTESTDIR, "test_record.xml"),
                                              parser).getroot())

        self.assertEqual(entity.role, "Record")
        # test whether the __role property of this object has explicitely been
        # set.
        self.assertEqual(getattr(entity, "_Entity__role"), "Record")


def test_parent_list():
    p1 = RecordType(name="A")
    pl = linkahead.common.models.ParentList([p1])
    assert p1 in pl
    assert pl.index(p1) == 0
    assert RecordType(name="A") not in pl
    assert RecordType(id=101) not in pl
    p2 = RecordType(id=101)
    pl.append(p2)
    assert p2 in pl
    assert len(pl) == 2
    p3 = RecordType(id=103, name='B')
    pl.append(p3)
    assert len(pl) == 3

    # test removal
    # remove by id only, even though element in parent list has name and id
    pl.remove(RecordType(id=103))
    assert len(pl) == 2
    assert p3 not in pl
    assert p2 in pl
    assert p1 in pl
    # Same for removal by name
    pl.append(p3)
    assert len(pl) == 3
    pl.remove(RecordType(name='B'))
    assert len(pl) == 2
    assert p3 not in pl
    # And an error if no suitable element can be found
    with raises(KeyError) as ve:
        pl.remove(RecordType(id=105, name='B'))
    assert "not found" in str(ve.value)
    assert len(pl) == 2

    # TODO also check pl1 == pl2


def test_property_list():
    # TODO: Resolve parent-list TODOs, then transfer to here.
    # TODO: What other considerations have to be done with properties?
    p1 = Property(name="A")
    pl = linkahead.common.models.PropertyList()
    pl.append(p1)
    assert p1 in pl
    assert Property(id=101) not in pl
    p2 = Property(id=101)
    pl.append(p2)
    assert p1 in pl
    assert p2 in pl
    p3 = Property(id=103, name='B')
    pl.append(p3)


def test_filter_by_identity():
    rt1 = RecordType(id=100)
    rt2 = RecordType(id=101, name="RT")
    rt3 = RecordType(name="")
    p1 = Property(id=100)
    p2 = Property(id=100)
    p3 = Property(id=101, name="RT")
    p4 = Property(id=102, name="P")
    p5 = Property(id=103, name="P")
    p6 = Property(name="")
    r1 = Record(id=100)
    r2 = Record(id=100)
    r3 = Record(id=101, name="RT")
    r4 = Record(id=101, name="R")
    r5 = Record(id=104, name="R")
    r6 = Record(id=105, name="R")
    test_ents = [rt1, rt2, rt3, p1, p2, p3, p4, p5, p6, r1, r2, r3, r4, r5, r6]

    # Setup
    for entity in [Property(name=""), Record(name=""), RecordType(name="")]:
        for coll in [entity.properties, entity.parents]:
            for ent in test_ents:
                assert ent not in coll
                assert ent not in coll.filter_by_identity(ent)

        # Checks with each type
        t, t_props, t_pars = entity, entity.properties, entity.parents
        # Properties
        # Basic Checks
        t.add_property(p1)
        tp1 = t.properties[-1]
        t.add_property(p3)
        tp3 = t.properties[-1]
        assert len(t_props.filter_by_identity(pid=100)) == 1
        assert tp1 in t_props.filter_by_identity(pid=100)
        assert len(t_props.filter_by_identity(pid="100")) == 1
        assert tp1 in t_props.filter_by_identity(pid="100")
        assert len(t_props.filter_by_identity(pid=101, name="RT")) == 1
        assert tp3 in t_props.filter_by_identity(pid=101, name="RT")
        for entity in [rt1, p2, r1, r2]:
            assert entity not in t_props.filter_by_identity(pid=100)
            assert tp1 in t_props.filter_by_identity(entity)
        # Check that direct addition (not wrapped) works
        t_props.append(p2)
        tp2 = t_props[-1]
        assert tp2 in t_props.filter_by_identity(pid=100)
        assert tp2 not in t_props.filter_by_identity(pid=101, name="RT")
        for entity in [rt1, r1, r2]:
            assert entity not in t_props.filter_by_identity(pid=100)
            assert tp2 in t_props.filter_by_identity(entity)

        # Parents
        # Filtering with both name and id
        t.add_parent(r3)
        tr3 = t.parents[-1]
        t.add_parent(r5)
        tr5 = t.parents[-1]
        assert tr3 in t_pars.filter_by_identity(pid=101)
        assert tr5 not in t_pars.filter_by_identity(pid=101)
        assert tr3 not in t_pars.filter_by_identity(name="R")
        assert tr5 in t_pars.filter_by_identity(name="R")
        assert tr3 in t_pars.filter_by_identity(pid=101, name="R")
        assert tr5 not in t_pars.filter_by_identity(pid=101, name="R")
        assert tr3 not in t_pars.filter_by_identity(pid=104, name="RT")
        assert tr5 in t_pars.filter_by_identity(pid=104, name="RT")
        assert tr3 not in t_pars.filter_by_identity(pid=105, name="T")
        assert tr5 not in t_pars.filter_by_identity(pid=105, name="T")
        # Works also without id / name and with duplicate parents
        for ent in test_ents:
            t.add_parent(ent)
        for ent in t_pars:
            assert ent in t_pars.filter_by_identity(ent)

    # Grid-Based
    r7 = Record()
    r7.add_property(Property()).add_property(name="A").add_property(name="B")
    r7.add_property(id=27).add_property(id=27, name="A").add_property(id=27, name="B")
    r7.add_property(id=43).add_property(id=43, name="A").add_property(id=43, name="B")
    assert len(r7.properties.filter_by_identity(pid=27)) == 3
    assert len(r7.properties.filter_by_identity(pid=43)) == 3
    assert len(r7.properties.filter_by_identity(pid=43, conjunction=True)) == 3
    assert len(r7.properties.filter_by_identity(name="A")) == 3
    assert len(r7.properties.filter_by_identity(name="B")) == 3
    assert len(r7.properties.filter_by_identity(name="B", conjunction=True)) == 3
    assert len(r7.properties.filter_by_identity(pid=1, name="A")) == 1
    assert len(r7.properties.filter_by_identity(pid=1, name="A", conjunction=True)) == 0
    assert len(r7.properties.filter_by_identity(pid=27, name="B")) == 4
    assert len(r7.properties.filter_by_identity(pid=27, name="B", conjunction=True)) == 1
    assert len(r7.properties.filter_by_identity(pid=27, name="C")) == 3
    assert len(r7.properties.filter_by_identity(pid=27, name="C", conjunction=True)) == 0
    # Entity based filtering behaves the same
    assert (r7.properties.filter_by_identity(pid=27) ==
            r7.properties.filter_by_identity(Property(id=27)))
    assert (r7.properties.filter_by_identity(pid=43, conjunction=True) ==
            r7.properties.filter_by_identity(Property(id=43), conjunction=True))
    assert (r7.properties.filter_by_identity(name="A") ==
            r7.properties.filter_by_identity(Property(name="A")))
    assert (r7.properties.filter_by_identity(name="B") ==
            r7.properties.filter_by_identity(Property(name="B")))
    assert (r7.properties.filter_by_identity(name="B", conjunction=True) ==
            r7.properties.filter_by_identity(Property(name="B"), conjunction=True))
    assert (r7.properties.filter_by_identity(pid=1, name="A") ==
            r7.properties.filter_by_identity(Property(id=1, name="A")))
    assert (r7.properties.filter_by_identity(pid=1, name="A", conjunction=True) ==
            r7.properties.filter_by_identity(Property(id=1, name="A"), conjunction=True))
    assert (r7.properties.filter_by_identity(pid=27, name="B") ==
            r7.properties.filter_by_identity(Property(id=27, name="B")))
    assert (r7.properties.filter_by_identity(pid=27, name="B", conjunction=True) ==
            r7.properties.filter_by_identity(Property(id=27, name="B"), conjunction=True))
    assert (r7.properties.filter_by_identity(pid=27, name="C") ==
            r7.properties.filter_by_identity(Property(id=27, name="C")))
    assert (r7.properties.filter_by_identity(pid=27, name="C", conjunction=True) ==
            r7.properties.filter_by_identity(Property(id=27, name="C"), conjunction=True))
    # Name only matching and name overwrite
    r8 = Record().add_property(name="A").add_property(name="B").add_property(name="B")
    r8.add_property(Property(name="A"), name="B")
    r8.add_property(Property(name="A", id=12), name="C")
    assert len(r8.properties.filter_by_identity(name="A")) == 1
    assert len(r8.properties.filter_by_identity(name="B")) == 3
    assert len(r8.properties.filter_by_identity(name="C")) == 1
    assert len(r8.properties.filter_by_identity(pid=12)) == 1

    with warnings.catch_warnings(record=True) as w:
        # Cause all warnings to always be triggered.
        warnings.simplefilter("always")

        r7.properties.filter(pid=34)
        assert issubclass(w[-1].category, DeprecationWarning)
        assert "This function was renamed" in str(w[-1].message)

        t.parents.filter(pid=234)
        assert issubclass(w[-1].category, DeprecationWarning)
        assert "This function was renamed" in str(w[-1].message)
