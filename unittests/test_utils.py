# coding: utf-8
#
# ** header v3.0
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Tests for linkahead.common.utils."""
from __future__ import unicode_literals

from linkahead.common.utils import xml2str
from linkahead.utils.escape import (escape_dquoted_text, escape_squoted_text)
from lxml.etree import Element


def test_xml2str():
    name = 'Björn'
    element = Element(name)
    serialized = xml2str(element)
    assert serialized == "<Björn/>\n"


def test_escape_quoted_text():
    assert escape_squoted_text("bla") == "bla"
    assert escape_squoted_text(r"bl\a") == r"bl\\a"
    assert escape_squoted_text("bl*a") == r"bl\*a"
    assert escape_squoted_text(r"bl*ab\\lab\*labla") == r"bl\*ab\\\\lab\\\*labla"
    assert escape_squoted_text("bl'a") == r"bl\'a"
    assert escape_dquoted_text('bl"a') == r'bl\"a'
